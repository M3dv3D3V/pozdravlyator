import os


API_TOKEN = os.getenv('TOKEN_POZDRAVLYATOR_BOT')
URL_AUTH: str = f'https://api.telegram.org/bot{API_TOKEN}'

RESPONSE_INTERVAL: int = 2

RANDOM_BOX_EMOJI_CODE: str = '\ud83c\udfb2'
ALLOWED_MESSAGES: list = [
    '/add',
    '/get_list',
    '/select',
    RANDOM_BOX_EMOJI_CODE,
    '/help',
    '/start',
    '/clear'
]

PATH_TO_DATABASE: str = 'database.db'

HOST: str = '0.0.0.0'
PORT: int = 33001