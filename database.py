import sqlite3

import config
import utils

import consts


class DBInterface:
    def __init__(self, path: str) -> None:
        self.__path_to_db = path
        self.__connection = sqlite3.connect(self.__path_to_db)
        self.__cursor = self.__connection.cursor()

    def perform_execute(self, command):
        self.__cursor.execute(command)
        self.__connection.commit()

    def _create_table_users(self):
        query: str = """CREATE TABLE IF NOT EXISTS users(
                        pk_id INTEGER PRIMARY KEY,
                        username TEXT,
                        chat_id TEXT);
                    """
        self.__cursor.execute(query)

    def add_user_into_users(self, username: str, chat_id: str):
        query: str = 'INSERT INTO users(username, chat_id) VALUES (?,?)'
        data_user: tuple = (username, chat_id)
        validation_query: str = f"SELECT * FROM users WHERE username='{username}' and chat_id='{chat_id}';"
        self.__cursor.execute(validation_query)
        if self.__cursor.fetchall():
            # TODO will added answer if user already exist
            return 'Already added'

        self.__cursor.execute(
            query, data_user
        )
        self.__connection.commit()

    def delete_user_by_username(self, username: str):
        query: str = f"DELETE FROM users WHERE username='{username}';"
        self.__cursor.execute(
            query
        )
        self.__connection.commit()

    def delete_users_by_chat_id(self, chat_id: str):
        query: str = f"DELETE FROM users WHERE chat_id='{chat_id}';"
        self.__cursor.execute(
            query
        )
        self.__connection.commit()

    def get_users_by_chat_id(self, chat_id: str):
        query: str = f"SELECT * FROM users WHERE chat_id='{chat_id}';"
        self.__cursor.execute(query)
        all_results = self.__cursor.fetchall()
        return all_results


def get_users(chat_id) -> list:
    db_connection = DBInterface(config.PATH_TO_DATABASE)
    result = db_connection.get_users_by_chat_id(str(chat_id))
    if not result:
        return []

    return [users[1] for users in result]


def add_users_into_users_table(users: list, chat_id: str):
    db_connection = DBInterface(config.PATH_TO_DATABASE)
    for user in users:
        db_connection.add_user_into_users(user, str(chat_id))

    utils.send_message_to_client(chat_id, consts.USERS_WAS_ADDED)


def delete_users_from_chat(chat_id: str):
    db_connection = DBInterface(config.PATH_TO_DATABASE)
    db_connection.delete_users_by_chat_id(chat_id)


if __name__ == '__main__':
    db_connection = DBInterface(config.PATH_TO_DATABASE)
    db_connection._create_table_users()
