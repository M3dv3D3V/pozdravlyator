from time import sleep

from flask import Flask
from flask import request
from flask import jsonify

import consts
import config
from database import add_users_into_users_table, delete_users_from_chat
from utils import (
    send_message_to_client,
    select_treasurer,
    get_treasurers_by_chat_id
)


APP = Flask(__name__)


@APP.route('/', methods=['POST', 'GET'])
def handler():
    if request.method == 'POST':
        json_object = request.get_json()
        if json_object.get('edited_message'):
            return jsonify(json_object)

        if json_object.get('message') is None:
            return jsonify(json_object)

        chat_id = json_object.get('message').get('chat').get('id')

        # cubic handler :)
        if json_object.get('message').get('dice') is not None:
            treasurer = select_treasurer(chat_id)
            send_message_to_client(chat_id, f'{consts.RANDOM_TREASURER_MESSAGE} {treasurer}')
            return jsonify(json_object)

        message: str = json_object.get('message').get('text')
        if message is None:
            return jsonify(json_object)

        denied_message: bool = message.split()[0] not in config.ALLOWED_MESSAGES

        emoji: str = ''
        if json_object.get('message').get('dice') is not None:
            emoji = json_object.get('message').get('dice')

        if denied_message and not emoji:
            return jsonify(json_object)

        message_for_adding_users = message.split(',')
        message: list = message.split()

        if message[0] == '/help' or message[0] == '/start':
            send_message_to_client(chat_id, consts.COMMANDS_LIST)

        if len(message) > 1 and message[0] == '/add':
            first_treasurer: str = message_for_adding_users[0][5:]
            treasurers: list = [first_treasurer] + message_for_adding_users[1:]
            add_users_into_users_table(treasurers, chat_id)

        if message[0] == '/get_list':
            treasurers: str = get_treasurers_by_chat_id(chat_id)
            send_message_to_client(chat_id, f'{consts.CURRENT_TREASURERS} {treasurers}')

        if message[0] == '/select':
            treasurer = select_treasurer(chat_id)
            send_message_to_client(chat_id, f'{consts.RANDOM_TREASURER_MESSAGE} {treasurer}')

        if message[0] == '/clear':
            delete_users_from_chat(str(chat_id))
            send_message_to_client(chat_id, consts.CLEAR_TREASURERS_LIST)

    return '<h1> hello bot </h1>'


if __name__ == '__main__':
    APP.run(host=config.HOST, port=config.PORT)
