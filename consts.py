COMMANDS_LIST: str = (
    'Список команд:%0A'
    '/add - добавить казначеев /add user1,user2,user3...%0A'
    '/get_list - получить список казначеев%0A'
    '/clear - очистить список казначеев%0A'
    '/select - выбрать казначея%0A'
    '/help или /start - список команд%0A'
)
RANDOM_TREASURER_MESSAGE: str = 'В этот раз казначеем становится - '
USERS_WAS_ADDED: str = 'Казначеи были добавлены.'
CURRENT_TREASURERS: str = 'Текущие казначеи:'
CLEAR_TREASURERS_LIST: str = 'Список казначеев был очищен.'