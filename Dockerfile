FROM ubuntu:18.04

EXPOSE 33001

RUN apt-get update -y && apt-get upgrade -y && \ 
    apt-get install -y python3 python3-pip

COPY . .

RUN pip3 install -r requirements.txt

CMD [ "python3", "server.py"]
