from random import choice
from time import sleep

import requests

import config
import database


def send_message_to_client(chat_id: str, message: str, timeout=None) -> None:
    url = f'{config.URL_AUTH}/sendMessage?chat_id={chat_id}&parse_mode=HTML&text={message}'
    headers: dict = {'User-Agent': get_random_user_agent()}
    requests.get(url, timeout=timeout, headers=headers)


def get_random_user_agent() -> str:
    with open('user_agents.txt', 'r') as file:
        user_agents = file.readlines()
        return choice(user_agents).strip()


def select_treasurer(chat_id: str) -> str:
    users_by_chat_id = database.get_users(chat_id)
    if not users_by_chat_id:
        return ''

    return choice(users_by_chat_id)


def get_treasurers_by_chat_id(chat_id: str) -> str:
    users = database.get_users(chat_id)
    if not users:
        return ''

    return ','.join(users)